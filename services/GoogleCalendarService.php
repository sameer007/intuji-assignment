<?php
$credentials = '../config/credentials.json';

interface GoogleCalendarInterface
{
	public function listEvents($credentials, $calendarId);
	public function createEvents($credentials, $dataToInsert);
	public function deleteEvents($credentials, $eventId);

	public function getClient($credentials);

}

class GoogleCalendarService implements GoogleCalendarInterface
{
	public $service;

	/**
	 * get list of events from google calendar using calendar ID
	 */
	public function listEvents($credentials, $calendarId)
	{
		try {
			$client = $this->getClient($credentials);
			$service = new Google_Service_Calendar($client);

			$response = $service->events->listEvents($calendarId);
			$res = [
				'events'=> $response
			];
			return $res;
		} catch (Exception $e) {
			$errorData = json_decode($e->getMessage(), true);
			$reason = $errorData['error']['errors'][0]['reason'];
			$res = [
				'error' => "An error occurred: " . $reason
			];
			return $res;
		}
	}

	/**
	 * Create new event in google calendat using credentials
	 */
	public function createEvents($credentials, $dataToInsert)
	{
		$client = $this->getClient($credentials);
		$service = new Google_Service_Calendar($client);

		$summary = $dataToInsert['summary'];
		$location = $dataToInsert['location'];
		$description = $dataToInsert['description'];

		$startDate = new DateTime($dataToInsert['startDate']);
		$startDate->setTime(9, 0);
		$startDate->setTimezone(new DateTimeZone('-07:00'));
		$formatted_startDate = $startDate->format('Y-m-d\TH:i:sP');

		$endDate = new DateTime($dataToInsert['endDate']);
		$endDate->setTime(9, 0);
		$endDate->setTimezone(new DateTimeZone('-07:00'));
		$formatted_endDate = $endDate->format('Y-m-d\TH:i:sP');

		$start = array(
			'dateTime' => $formatted_startDate,
			'timeZone' => 'America/Los_Angeles',
		);
		$end = array(
			'dateTime' => $formatted_endDate,
			'timeZone' => 'America/Los_Angeles',
		);

		$event = new Google_Service_Calendar_Event(
			array(
				'summary' => $summary,
				'location' => $location,
				'description' => $description,
				'start' => $start,
				'end' => $end,
				'recurrence' => array(
					'RRULE:FREQ=DAILY;COUNT=2'
				),
				'attendees' => array(),
				'reminders' => array(
					'useDefault' => FALSE,
					'overrides' => array(
						array('method' => 'email', 'minutes' => 24 * 60),
						array('method' => 'popup', 'minutes' => 10),
					),
				),
			)
		);
		try {
			$event = $service->events->insert($_SESSION['calendarId'], $event);
			if (isset($event) && !empty($event)) {
				$res = [
					'success' => 'success',
					'message' => 'Google Calendar Event Created Successfully'
				];
				return $res;
			}
		} catch (Google_Service_Exception $e) {
			$errorData = json_decode($e->getMessage(), true);
			$reason = $errorData['error']['errors'][0]['reason'];
			
			$res = [
				'error' => "An error occurred: " . $reason
			];
			return $res;

		}

	}
	/**
	 * Delete event in google calendar using event ID
	 */
	public function deleteEvents($credentials, $eventId)
	{
		try {
			$client = $this->getClient($credentials);
			$service = new Google_Service_Calendar($client);

			$isDeleted = $service->events->delete($_SESSION['calendarId'], $eventId);
			if (isset($isDeleted) && !empty($isDeleted)) {
				$res = [
					'success' => 'success',
					'message' => 'Event Deleted Successfully'
				];
			} else {
				$res = [
					'error' => 'Something went Wrong while deleting event',
				];
			}

			return $res;
		} catch (Exception $e) {
			$errorData = json_decode($e->getMessage(), true);
			$reason = $errorData['error']['errors'][0]['reason'];
			$res = [
				'error' => "An error occurred: " . $reason
			];
			return $res;
		}


	}
	/**
	 * Get client for creation,deletion and listing of google calendar events
	 */
	public function getClient($credentials)
	{
		$client = new Google_Client();
		$client->setApplicationName('testoo');
		$client->setScopes(array(Google_Service_Calendar::CALENDAR));
		$client->setAuthConfig($credentials);
		$client->setAccessType('offline');
		$client->getAccessToken();
		$client->getRefreshToken();
		return $client;
	}
}