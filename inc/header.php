<?php 

$routeArray = array();
$backRoute = './';
$current_page = pathinfo($_SERVER['PHP_SELF'], PATHINFO_FILENAME);
$requestUriRoutes = array_values(array_filter(explode('/', $_SERVER['PHP_SELF'])));

$i = 0;

foreach ($requestUriRoutes as $key => $value) {
  if($requestUriRoutes[0] == 'backoffice'){
    if($key > 1){
      $backRoute .= '../';
    }
  }else{
    if($key > 0){
      $backRoute .= '../';
    }
  }
  
  $routeArray['backRoute'][] = $backRoute;
  $routeArray['route'][] = $value;
  if(++$i === count($requestUriRoutes)){
    $currentPageBackRoute = $backRoute;

  }else{
    $currentPageBackRoute = './';
  }
}
define('CURRENT_PAGE_BACK_ROUTE', $currentPageBackRoute);
//echo CURRENT_PAGE_BACK_ROUTE;
require 'config/config.php';
require 'config/functions.php';


?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo SITE_TITLE ?></title>
  <link rel="shortcut icon" type="image/jpg" href="<?php echo ASSETS_URL ?>dist/img/AdminLTELogo.png"/>   
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo ASSETS_URL ?>plugins/fontawesome-free/css/all.min.css">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?php echo ASSETS_URL ?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- Select 2 -->
  <link rel="stylesheet" href="<?php echo ASSETS_URL ?>plugins/select2/css/select2.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?php echo ASSETS_URL ?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <!-- JQVMap -->
  <link rel="stylesheet" href="<?php echo ASSETS_URL ?>plugins/jqvmap/jqvmap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo ASSETS_URL ?>dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?php echo ASSETS_URL ?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?php echo ASSETS_URL ?>plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?php echo ASSETS_URL ?>plugins/summernote/summernote-bs4.min.css">
  <!-- custom css -->
  <link rel="stylesheet" href="<?php echo ASSETS_URL ?>css/style.css">
  <!-- progressbar -->

  <!-- jQuery -->
  <script src="<?php echo ASSETS_URL ?>plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="<?php echo ASSETS_URL ?>plugins/jquery-ui/jquery-ui.min.js"></script>
  <script src="<?php echo ASSETS_URL ?>js/functions.js"></script>
  
</head>
<body>