<?php $footerDisabledPages = array('index','register'); ?>

  <footer class="main-footer">
    <strong>Copyright &copy; 2014-2021 <a href="https://adminlte.io">AdminLTE.io</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 3.1.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

</div>
<!-- ./wrapper -->

<!-- Bootstrap 4 -->
<script src="<?php echo ASSETS_URL ?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Sparkline -->
<script src="<?php echo ASSETS_URL ?>plugins/sparklines/sparkline.js"></script>

<!-- JQVMap -->
<script src="<?php echo ASSETS_URL ?>plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="<?php echo ASSETS_URL ?>plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- jQuery Knob Chart -->
<script src="<?php echo ASSETS_URL ?>plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?php echo ASSETS_URL ?>plugins/moment/moment.min.js"></script>
<script src="<?php echo ASSETS_URL ?>plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?php echo ASSETS_URL ?>plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?php echo ASSETS_URL ?>plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?php echo ASSETS_URL ?>plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo ASSETS_URL ?>dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- <script src="<?php echo ASSETS_URL ?>dist/js/demo.js"></script> -->
<script src="<?php echo ASSETS_URL ?>dist/js/adminlte.min.js"></script>

<?php   
if(isset($scripts) && !empty($scripts)){
  echo $scripts;
} 
?>


</body>
</html>