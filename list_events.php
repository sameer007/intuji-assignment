<?php
include './inc/header.php';
include './inc/session.php';
include './services/GoogleCalendarService.php';
require './vendor/autoload.php';
$credentials = './config/credentials.json';

$calendarId = $_SESSION['calendarId'];
$googleCalendarService = new GoogleCalendarService();
$eventListResponse = $googleCalendarService->listEvents($credentials, $calendarId);

//flash error if there is error in retreiving events
if(isset($eventListResponse['error']) && !empty( $eventListResponse['error'])) {
  $_SESSION['error'] = $eventListResponse['error'].". Your calendar ID might be incorrect.Please logout and login using different Calendar ID!!";
}
?>

<div class="wrapper">
  <?php include './inc/left-sidebar.php'; ?>
  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>

              <?php if (isset($routeArray) && !empty($routeArray)) {

                displayRoutes($routeArray);
              }
              ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Event Lists</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <script src="<?php echo ASSETS_URL ?>js/customJSLibrary.js"></script>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div id="row">
          <div class="col-lg-12">
            <table class="table table-striped">
              <th>S.N</th>
              <th>Event Name</th>
              <th>Description</th>
              <th>Location</th>
              <th>Start</th>
              <th>End</th>
              <th>Action</th>

              <?php
              if (isset($eventListResponse['events']) && !empty($eventListResponse['events'])) {
                foreach ($eventListResponse['events']->getItems() as $key => $event) {

                  ?>
                  <tr>
                    <td>
                      <?php echo ($key + 1); ?>
                    </td>
                    <td>
                      <?php echo $event->getSummary() ?>
                    </td>
                    <td>
                      <?php echo $event->getDescription() ?>
                    </td>
                    <td>
                      <?php echo $event->getLocation() ?>
                    </td>
                    <td>
                      <?php echo $event->getStart()->dateTime ?>
                    </td>
                    <td>
                      <?php echo $event->getEnd()->dateTime ?>
                    </td>
                    <td>
                      <?php
                      $deleteEvent_url = 'process/event?eventId=' . $event->id . '&act=' . substr(md5('del-event-' . $event->id), 5, 15);
                      ?>
                      <a type="button" class="btn btn-danger" href="<?php echo $deleteEvent_url; ?>"
                        onclick="return confirm('Are You sure you want to delete this Google Calendar Event?');">Delete</a>
                    </td>
                  </tr>
                  <?php
                }
              }
              ?>
            </table>
          </div>
        </div>
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <?php

  include './inc/footer.php';
  ?>