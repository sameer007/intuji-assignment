<?php
include './inc/header.php';
include './inc/session.php';



?>

<div id="login-page">
  <div class="container">

    <form class="form-login" id="myForm" action="process/login" method="post">


      <h2 class="form-login-heading">Login now</h2>
      <div class="content-header flash">
        <div class="container-fluid flash">
          <div class="row">
            <div class="col-auto">
              <?php flash(); ?>
            </div>
          </div>
        </div>
      </div>
      <div class="login-wrap">
        <input type="email" name="calendarId" class="form-control" placeholder="Enter Calendar ID">
        <br>
        <button type="submit" name="login" value="submit" class="btn btn-primary btn-block"><i class="fa fa-lock"></i>
          SIGN
          IN</button>
        <hr>
      </div>
    </form>
  </div>
</div>
<script type="text/javascript" src="lib/jquery.backstretch.min.js"></script>
<script>
  $.backstretch("{{url('img/instagram.jpg')}}", {
    speed: 500
  });
</script>
<?php
$scripts = '
';

include './inc/footer.php';
?>