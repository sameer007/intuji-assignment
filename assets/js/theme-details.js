$( document ).ready(function() {
	$('#collapseDetails').on('shown.bs.collapse', function () {
        // do something…
        console.log('here shown');
        detailsCollapseControl = $('#detailsCollapseControl')

        detailsCollapseControl.addClass('btn-primary');
        detailsCollapseControl.removeClass('btn-default');
        console.log(detailsCollapseControl[0])

    })
  $('#collapseDetails').on('hidden.bs.collapse', function () {
        // do something…

        detailsCollapseControl = $('#detailsCollapseControl')
        detailsCollapseControl.addClass('btn-default');
        detailsCollapseControl.removeClass('btn-primary');
    })
  $('#collapseScreenshot').on('shown.bs.collapse', function () {
        // do something…
        screenshotCollapseControl = $('#screenshotCollapseControl')

        screenshotCollapseControl.addClass('btn-primary');
        screenshotCollapseControl.removeClass('btn-default');

    })
  $('#collapseScreenshot').on('hidden.bs.collapse', function () {
        // do something…

        screenshotCollapseControl = $('#screenshotCollapseControl')
        screenshotCollapseControl.addClass('btn-default');
        screenshotCollapseControl.removeClass('btn-primary');
    })
  $('#collapseDocumentation').on('shown.bs.collapse', function () {
        // do something…
        documentationCollapseControl = $('#documentationCollapseControl')

        documentationCollapseControl.addClass('btn-primary');
        documentationCollapseControl.removeClass('btn-default');

    })
  $('#collapseDocumentation').on('hidden.bs.collapse', function () {
        // do something…
        console.log('here hidden');

        documentationCollapseControl = $('#documentationCollapseControl')
        documentationCollapseControl.addClass('btn-default');
        documentationCollapseControl.removeClass('btn-primary');
    })
});