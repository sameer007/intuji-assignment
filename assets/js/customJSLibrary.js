class CustomJSLibrary {
  
  constructor() {
    
  }
  // Get datasets from a DOM
  getElement(elementIdOrClass) {
    let annotation = elementIdOrClass[0];
    if(annotation == '.'){
      //get By class
      let className = elementIdOrClass.split('.').slice(-1)[0];

      return document.getElementsByClassName(className);
    }else if(annotation == '#'){
      //get By id
      let elementId = elementIdOrClass.split('#').slice(-1)[0];
      console.log('elementId>>',elementId);

      return document.getElementById(elementId);
    }else {
      return null ;
    }
  }

  // Change class name of a DOM
  changeClass(element, className) {
    element.classList.add(className);
  }

  // Get datasets from a DOM
  getDatasets(element) {
    return element.dataset;
  }

  // Inject new DOM element
  injectElement(parent, child) {
    parent.appendChild(child);
  }

  // Make AJAX request
  makeAjaxRequest(url, options) {
    return new Promise((resolve, reject) => {
      const xhr = new XMLHttpRequest();
      xhr.open(options.method || 'GET', url, true);
      xhr.onload = () => {
        if (xhr.status >= 200 && xhr.status < 300) {
          resolve(xhr.responseText);
        } else {
          reject(xhr.statusText);
        }
      };
      xhr.onerror = () => reject(xhr.statusText);
      xhr.send(options.data || null);
    });
  }

  makeMultiplePostRequest(url1,url2,url3){

    var data = {
      "name": "morpheus",
      "job": "leader"
    };
    const options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(data),
    };
    const request1 = fetch(url1, options);
    const request2 = fetch(url2, options);
    const request3 = fetch(url3, options);

    Promise.all([request1, request2, request3])
    .then(responses => {
      // Handle the responses for all three requests here
      const response1 = responses[0];
      const response2 = responses[1];
      const response3 = responses[2];
      
      if (response1.ok) {
        // Process response for request 1
        return response1.json();
      } else {
        throw new Error('Request 1 failed');
      }
    })
    .then(data => {
      // Handle the data from request 1
      console.log('Data from request 1:', data);
      
      // You can do the same for request 2 and request 3
    })
    .catch(error => {
      // Handle errors here
      console.error('Error:', error);
    });
  }

  // Make a GET request
  getRequest(url) {
    return this.makeAjaxRequest(url, { method: 'GET' });
  }

  checkRadioElement(element) {
    element.setAttribute('checked','checked');
  }

  uncheckRadioElement(element) {
      element.removeAttribute('checked');
  }

  // Set a value for an input, checkbox, or select dropdown
  setElementValue(element, value) {
    if (element.tagName === 'INPUT' || element.tagName === 'SELECT') {
      element.value = value;
    }
  }

  // Get the value from an input, checkbox, or select dropdown
  getElementValue(element) {
    if (element.tagName === 'INPUT' || element.tagName === 'SELECT') {
      return element.value;
    }
    return null;
  }
}