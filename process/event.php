<?php 
require '../config/config.php';


require $_SERVER['DOCUMENT_ROOT'].ROOT.'/config/functions.php';

include '../services/GoogleCalendarService.php';

require '../vendor/autoload.php';
$credentials = '../config/credentials.json';

if(isset($_POST) && !empty($_POST)){
	/**
	 * Controller to add google calendar event
	 */
	if (isset($_POST['submit']) && !empty($_POST['submit']) && $_POST['submit'] == 'addEvent') {
		$eventInfo  = array();
		$eventInfo['summary'] = sanitize($_POST['summary']);
		$eventInfo['location'] = sanitize($_POST['location']);
		$eventInfo['description'] = sanitize($_POST['description']);
		$eventInfo['startDate'] = sanitize($_POST['startDate']);
		$eventInfo['endDate'] = sanitize($_POST['endDate']);

		$googleCalendarService = new GoogleCalendarService();

		$response = $googleCalendarService->createEvents($credentials,$eventInfo);

		if (isset($response['message']) && !empty($response['message'])) {
			redirect('../list_events','success',$response['message']);
		}else{
			redirect('../create_events','error',$response['error'].". Something went wrong while creating event");
		}
	}else{
		redirect('../', 'error','Unauthorized access');
	}
}elseif(isset($_GET) && !empty($_GET)){
	if (isset($_GET['eventId']) && !empty($_GET['eventId'])) {
		/**
		* Controller to delete google calendar event
		*/
		if($_GET['act'] == substr(md5('del-event-'.$_GET['eventId']), 5, 15)){
			$eventId = $_GET['eventId'];
			$googleCalendarService = new GoogleCalendarService();
			$response = $googleCalendarService->deleteEvents($credentials,$eventId);

			if (isset($response['message']) && !empty($response['message'])) {
				
				redirect('../list_events','success', 'Event deleted successfully!.');

			}else{
				redirect('../list_events','error',$response['error']);

			}
		}else{
			redirect('../404');
		}
	}else{
		redirect('../404');
	}
} else {
	redirect('../', 'error','Unauthorized access');
}