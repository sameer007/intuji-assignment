<?php 
require '../config/config.php';
require $_SERVER['DOCUMENT_ROOT'].ROOT.'/config/functions.php';

if(isset($_POST) && !empty($_POST)){
	/**
	 * Login using calendar ID
	 */
	if (isset($_POST['login']) && !empty($_POST['login']) && $_POST['login'] == 'submit') {

        $_SESSION['calendarId'] = sanitize($_POST['calendarId']);

		redirect('../list_events'.$eventListUrl,'success','Logged in Successfully to calendarID: `'.$_SESSION['calendarId'].'`');
		
	}else{
		redirect('../', 'error','Unauthorized access');
	}
} else {
	redirect('../', 'error','Unauthorized access');
}