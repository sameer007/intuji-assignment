<?php 
include './inc/header.php';
include './inc/session.php';

?>

<div class="wrapper">
  <?php include './inc/left-sidebar.php';?>
  <!-- Content Wrapper. Contains page content -->

  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header flash">
      <div class="container-fluid flash">
        <div class="row">
          <div class="col-auto">
            <?php flash(); ?>
          </div>
        </div>
      </div>
    </div>

    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-left">
              <div class="circle-back">
                <i class="far fa-arrow-alt-circle-left fa-lg"></i>
              </div>

              <?php  if(isset($routeArray) && !empty($routeArray)){

                displayRoutes($routeArray);
              }
              ?>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Create Google Calendar Event</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
    <script src="<?php echo ASSETS_URL ?>js/customJSLibrary.js"></script>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div id="row">



          <form id="eventsForm" class="form-horizontal style-form" action="process/event" METHOD="POST">
            <div class="form-group">
              <label>Event Summary :</label>
              <input type="text" name="summary">
            </div>
            <div class="form-group">

              <label>Event Location :</label>
              <input type="text" name="location">
            </div>
            <div class="form-group">

              <label>Event Description :</label>
              <textarea name="description"></textarea>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label"> Start Date</label>
              <div class="col-sm-3">
                <input type="date" name="startDate" id="startDate" class="form-control" required>
                <span class="help-block"></span>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 col-sm-2 control-label">End Date</label>
              <div class="col-sm-3">
                <input type="date" name="endDate" id="endDate" class="form-control" required>
                <span class="help-block"></span>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="form-group col-md-6"><button class="btn btn-primary" type="submit" name="submit" value="addEvent">Submit</button></div>
            <div class="clearfix"></div>
          </form>
        </div>
      </div>
    </div>
    <!-- /.container-fluid -->

  </section>
  <!-- /.content -->
</div>
<script type="text/javascript" src="public/lib/jquery/jquery.min.js"></script>
<script type="text/javascript" src="public/lib/bootstrap/js/bootstrap.min.js"></script>
<?php 
include './inc/footer.php';
?>
