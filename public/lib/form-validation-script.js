var Script = function () {

    $.validator.setDefaults({
        submitHandler: function() { alert("submitted!"); }
    });

    $().ready(function() {
        $("#companyDetailsForm").validate({
            rules: {
                symbol: {
                    required: true,
                },
                startDate: {
                    required: true,
                    date: true,
                    maxDate: new Date()
                },
                endDate: {
                    required: true,
                    date: true,
                    maxDate: new Date(),
                    greaterThanOrEqual: "#startDate"
                },
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                symbol: {
                    required: "Company Symbol is required",
                },
                startDate: {
                    required: "Start Date is required",
                    date: "Invalid Start Date",
                    maxDate: "Start Date cannot be greater than current date"
                },
                endDate: {
                    required: "End Date is required",
                    date: "Invalid End Date",
                    maxDate: "End Date cannot be greater than current date",
                    greaterThanOrEqual: "End Date must be greater than or equal to Start Date"
                },
                email: {
                    required: "Email is required",
                    email: "Invalid Email"
                }
            }
        });
    });


}();